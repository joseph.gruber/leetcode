#
# @lc app=leetcode id=2 lang=python3
#
# [2] Add Two Numbers
#

# @lc code=start
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None


class Solution:
  def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
    tail = result = ListNode(0)
    carry = 0

    while l1 or l2 or carry:
      val1 = (l1.val if l1 else 0)
      val2 = (l2.val if l2 else 0)
      carry, val = divmod(val1 + val2 + carry, 10)

      tail.next = ListNode(val)
      tail = tail.next

      l1 = (l1.next if l1 else None)
      l2 = (l2.next if l2 else None)

    return result.next

# @lc code=end
