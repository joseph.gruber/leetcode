#
# @lc app=leetcode id=125 lang=python3
#
# [125] Valid Palindrome
#
# https://leetcode.com/problems/valid-palindrome/description/
#
# algorithms
# Easy (32.59%)
# Likes:    734
# Dislikes: 2140
# Total Accepted:    428.7K
# Total Submissions: 1.3M
# Testcase Example:  '"A man, a plan, a canal: Panama"'
#
# Given a string, determine if it is a palindrome, considering only
# alphanumeric characters and ignoring cases.
#
# Note: For the purpose of this problem, we define empty string as valid
# palindrome.
#
# Example 1:
#
#
# Input: "A man, a plan, a canal: Panama"
# Output: true
#
#
# Example 2:
#
#
# Input: "race a car"
# Output: false
#
#
#

# @lc code=start


class Solution:
  def isPalindrome(self, s: str) -> bool:
    import re

    pattern = re.compile('[\W_]+')
    s = pattern.sub('', s.lower())
    return s[::-1] == s

# @lc code=end
